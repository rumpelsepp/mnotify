module codeberg.org/rumpelsepp/mnotify

go 1.16

require (
	github.com/kr/text v0.2.0 // indirect
	github.com/lib/pq v1.10.2 // indirect
	github.com/mattn/go-isatty v0.0.13 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/pelletier/go-toml v1.9.3
	github.com/spf13/cobra v1.2.1
	github.com/tidwall/gjson v1.8.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
	github.com/tidwall/sjson v1.1.7 // indirect
	golang.org/x/crypto v0.0.0-20210813211128-0a44fdfbc16e
	golang.org/x/mod v0.5.0 // indirect
	golang.org/x/net v0.0.0-20210813160813-60bc85c4be6d // indirect
	golang.org/x/sys v0.0.0-20210816183151-1e6c022a8912 // indirect
	golang.org/x/term v0.0.0-20210615171337-6886f2dfbf5b // indirect
	golang.org/x/tools v0.1.5 // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	maunium.net/go/maulogger/v2 v2.3.0 // indirect
	maunium.net/go/mautrix v0.9.19
	modernc.org/cc/v3 v3.33.11 // indirect
	modernc.org/ccgo/v3 v3.10.0 // indirect
	modernc.org/memory v1.0.5 // indirect
	modernc.org/sqlite v1.12.0
)
